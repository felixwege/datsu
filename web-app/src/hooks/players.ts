import { useQuery, UseQueryResult } from "@tanstack/react-query";
import { getAllPlayers, getPlayer } from "@/services/player";
import { Player } from "@/models/player";

export const useAllPlayers = (): UseQueryResult<{ [id: string]: Player }> => {
  const queryKey = ["players", "allPlayers"];
  const queryFn = async () => {
    const players = await getAllPlayers();
    return players;
  };

  return useQuery(queryKey, queryFn);
};

// TODO: There must be a smart way to cache each individual player
export const usePlayers = (ids: string[]): UseQueryResult<{ [id: string]: Player }> => {
  const queryKey = ["players", { ...ids }];
  const queryFn = async () => {
    const players = await Promise.all(ids.map((id) => getPlayer(id)));
    const playerMap = Object.fromEntries(
      players.map((player) => [player.id, player])
    );
    return playerMap;
  };

  return useQuery(queryKey, queryFn);
};
