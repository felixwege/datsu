import {
  useQuery,
  useQueryClient,
  UseQueryResult,
} from "@tanstack/react-query";
import { useAllPlayers } from "./players";
import { GAMES_COLLECTION, gameConverter } from "@/services/game";
import { getPlayer } from "@/services/player";
import { useEffect, useMemo } from "react";
import {
  collection,
  doc,
  getFirestore,
  onSnapshot,
  query,
  where,
} from "firebase/firestore";
import { Game, PlayerData, STATS_CHECKOUT, StatsCheckout } from "@/models/game";
import { useRouter } from "next/router";
import { isDefined } from "@/utils/filter";
import {
  containsBust,
  isBreakfast,
  scoreAbbr,
  scoreFromDarts,
  scoreValueFromDartsScore,
  turnsFromDarts,
} from "@/utils/dartsScore";
import { DateTime } from "luxon";
import { toFirestoreTimestamp } from "@/utils/firestore";
import { PlayerStats } from "@/models/stats";
import { useUser } from "./user";
import assert from "assert";
import { ALL_POSSIBLE_SCORES } from "@/models/score";
import { buildPlayerStats } from "@/utils/stats";

export const useCurrentGameId = () => {
  const router = useRouter();
  const queryKey = "gameId";
  const gameId =
    router.query[queryKey] ||
    router.asPath.match(new RegExp(`[&?]${queryKey}=(.*)(&|$)`))?.[1];
  if (!gameId || typeof gameId !== "string") {
    throw new Error("Game id not found");
  }
  return gameId;
};

let hasGameListener = false;
// TODO Do not use firestore here! Use service instead.
export const useGame = (
  gameId: string | undefined,
): UseQueryResult<Game, unknown> => {
  const queryClient = useQueryClient();
  const queryKey = useMemo(() => ["games", gameId], [gameId]);

  useEffect(() => {
    if (gameId !== undefined && !hasGameListener) {
      const db = getFirestore();
      const ref = doc(db, GAMES_COLLECTION, gameId!).withConverter(
        gameConverter,
      ); // TODO check null instaed of !
      const unsubscribe = onSnapshot(ref, (value) => {
        queryClient.setQueryData(queryKey, value.data());
      });
      hasGameListener = true;

      return () => {
        hasGameListener = false;
        return unsubscribe();
      };
    }
  }, [gameId, queryClient, queryKey]);

  return useQuery(queryKey, () => new Promise(() => { }));
};

export const useCurrentGame = () => {
  const gameId = useCurrentGameId();
  return useGame(gameId);
};

// Returns players in game order
export const useGamePlayerDatas = (): PlayerData[] => {
  const { data: game } = useCurrentGame();
  if (!game) {
    return [];
  }
  return Object.values(game.players).sort((a, b) => a.position - b.position);
};

// Players in the current game
export const useGamePlayers = () => {
  const playerIds = useGamePlayerDatas().map((player) => player.playerId);
  const queryKey = ["players", { ...playerIds }];

  const queryFn = async () => {
    const playerPromises = playerIds.map(async (id) => {
      const player = await getPlayer(id);
      return player;
    });
    const players = await Promise.all(playerPromises);
    const playersMap = Object.fromEntries(
      players.map((player) => [player.id, player]),
    );
    return playersMap;
  };

  return useQuery(queryKey, queryFn);
};

export const useAvailablePlayers = () => {
  const currentPlayerIds = useGamePlayerDatas().map(
    (player) => player.playerId,
  );
  const allPlayers = useAllPlayers();

  if (!currentPlayerIds || !allPlayers.data) {
    return {};
  }

  const availablePlayers = Object.fromEntries(
    Object.entries(allPlayers.data).filter(
      ([id]) => !currentPlayerIds.includes(id),
    ),
  );

  return availablePlayers;
};

export const useCurrentPlayerData = () => {
  const { data: game } = useCurrentGame();
  if (!game) {
    return null;
  }
  const players = Object.values(game.players);

  const unfinishedTurnPlayers = players.filter(
    (playerData) => playerData.darts.length % 3 !== 0,
  );
  if (unfinishedTurnPlayers.length > 1) {
    console.error("More than one player has an unfinished turn");
  }

  if (unfinishedTurnPlayers.length === 1) {
    return unfinishedTurnPlayers[0];
  }

  let leastThrows = Infinity;
  players.forEach((playerData) => {
    leastThrows =
      playerData.darts.length < leastThrows
        ? playerData.darts.length
        : leastThrows;
  });

  const nextPlayer = players
    .filter((playerData) => playerData.darts.length === leastThrows)
    .sort(({ position: a }, { position: b }) => a - b)[0];

  return nextPlayer;
};

// relative position of -1 is the previous player, 1 is the next player
export const useRelativePlayerDataGetter = () => {
  const playerDatas = useGamePlayerDatas();
  const currentPlayerData = useCurrentPlayerData();

  return (relativePosition: number) => {
    if (!playerDatas || !currentPlayerData) {
      return null;
    }

    const cleanedSearchPosition =
      (currentPlayerData.position + relativePosition) % playerDatas.length;
    const searchPosition =
      cleanedSearchPosition < 0
        ? playerDatas.length + cleanedSearchPosition
        : cleanedSearchPosition;
    return playerDatas.find(
      (playerData) => playerData.position === searchPosition,
    );
  };
};

export const useLastThrowPlayerData = () => {
  const currentPlayerData = useCurrentPlayerData();
  const relativePlayerDataGetter = useRelativePlayerDataGetter();
  if (!currentPlayerData) {
    return null;
  }
  return currentPlayerData.darts.length % 3 === 0
    ? relativePlayerDataGetter(-1)
    : currentPlayerData;
};

export const usePlayerScore = (playerId: string | undefined) => {
  const { data: game } = useCurrentGame();
  if (!game || !playerId) {
    return null;
  }

  const playerData = game.players[playerId];
  const score = scoreFromDarts(playerData.darts);
  return score;
};

export const usePlayerRemainingScore = (playerId: string | undefined) => {
  const currentScore = usePlayerScore(playerId);
  const { data: game } = useCurrentGame();
  if (!game || !isDefined(currentScore)) {
    return null;
  }

  const remainingScore = game.gameSettings.score - currentScore;
  return remainingScore;
};

export const usePlayerNumberOfDarts = (playerId: string | undefined) => {
  const { data: game } = useCurrentGame();
  if (!game || !playerId) {
    return null;
  }

  const playerData = game.players[playerId];
  const nonNullDarts = playerData.darts.filter((it) => it !== null);
  return nonNullDarts.length;
};

export const usePlayerAverageScore = (playerId: string | undefined) => {
  const score = usePlayerScore(playerId);
  const numberOfDarts = usePlayerNumberOfDarts(playerId);
  if (!score || !numberOfDarts) {
    return 0;
  }

  // This is the average per dart but we want per turn
  return (score / numberOfDarts) * 3;
};

export const usePlayerLastMove = (playerId: string | undefined) => {
  const { data: game } = useCurrentGame();
  if (!game || !playerId) {
    return null;
  }

  const darts = game.players[playerId].darts;
  return game.players[playerId].darts.slice(
    darts.length % 3 === 0 ? -3 : -(darts.length % 3),
  );
};

let hasGamesListener = false;
// TODO Do not use firestore here! Use service instead.
export const useGames = (
  playerIds: string[],
  since: DateTime | undefined = undefined,
): UseQueryResult<{ [playerId: string]: Game[] }, unknown> => {
  assert(playerIds.length <= 10, "Firestore limit for array contains any");
  const sinceString = since?.toISO();
  const queryClient = useQueryClient();
  // TODO is it ok to use playerIds array as key?
  const queryKey = useMemo(
    () => ["games", playerIds, sinceString],
    // TODO: I don't get it. Eslint complains about spreading the player ids
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [...playerIds, sinceString],
  );

  useEffect(() => {
    if (!hasGamesListener) {
      const db = getFirestore();
      let q = query(
        collection(db, GAMES_COLLECTION).withConverter(gameConverter),
        where("playerIds", "array-contains-any", playerIds),
      );

      if (since) {
        q = query(q, where("createdAt", ">=", toFirestoreTimestamp(since)));
      }

      const unsubscribe = onSnapshot(q, (value) => {
        const games = value.docs.map((doc) => doc.data());
        const playerToGames = Object.fromEntries(
          playerIds.map((playerId) => {
            const playerGames = games.filter((game) =>
              game.playerIds.includes(playerId),
            );
            return [playerId, playerGames];
          }),
        );
        queryClient.setQueryData(queryKey, playerToGames);
      });
      hasGamesListener = true;

      return () => {
        hasGamesListener = false;
        return unsubscribe();
      };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [queryClient, queryKey]);

  return useQuery(queryKey, () => new Promise(() => { }));
};

export const usePlayersStats = (
  playerIds: string[],
  since: DateTime | undefined = undefined,
): { [playerId: string]: { [checkout: string]: PlayerStats } } => {
  const { data: games } = useGames(playerIds, since);
  if (!games) {
    return {};
  }
  const playerStats = Object.fromEntries(
    playerIds.map((playerId) => [
      playerId,
      buildPlayerStats(playerId, games[playerId]),
    ]),
  );
  return playerStats;
};

export const useRecentUnfinishedGames = () => {
  const user = useUser();
  const { data: games } = useGames(
    [user.player.id],
    DateTime.now().minus({ days: 2 }).startOf("day"),
  );
  if (!games || !games[user.player.id]) {
    return [];
  }

  return games[user.player.id].filter((game) => game.state === "IN_PROGRESS");
};

let hasCurrentGamesListener = false;
// TODO Do not use firestore here! Use service instead.
export const useCurrentGames = (): UseQueryResult<Game[], unknown> => {
  const queryClient = useQueryClient();
  const queryKey = "current_games";

  useEffect(() => {
    if (!hasCurrentGamesListener) {
      const db = getFirestore();
      let q = query(
        collection(db, GAMES_COLLECTION).withConverter(gameConverter),
        where("state", "!=", "FINISHED"),
      );

      const unsubscribe = onSnapshot(q, (value) => {
        const games = value.docs.map((doc) => doc.data());
        queryClient.setQueryData([queryKey], games);
      });
      hasCurrentGamesListener = true;

      return () => {
        hasCurrentGamesListener = false;
        return unsubscribe();
      };
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [queryClient, queryKey]);

  return useQuery([queryKey], () => new Promise(() => { }));
};

export const useCurrentGamesWithPlayers = () => {
  const { data: games } = useCurrentGames();
  if (!games) {
    return [];
  }

  return games.filter((game) => game.playerIds.length);
};
