import { UserContext } from "@/components/loginGuard";
import { useContext } from "react";

export const useUser = () => useContext(UserContext)();
