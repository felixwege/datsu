import { Player } from "@/models/player";
import {
  collection,
  doc,
  getDoc,
  getDocs,
  getFirestore,
  query,
  where,
  FirestoreDataConverter
} from "firebase/firestore";

const PLAYERS_COLLECTION = "players";

export const getAllPlayers = async (): Promise<{ [id: string]: Player }> => {
  const db = getFirestore();
  const querySnapshot = await getDocs(
    collection(db, PLAYERS_COLLECTION).withConverter(playerConverter),
  );
  const players: { [id: string]: Player } = {};
  for (const doc of querySnapshot.docs) {
    const player = doc.data()!;
    players[doc.id] = player;
  }

  return players;
};

export const getPlayer = async (id: string): Promise<Player> => {
  const db = getFirestore();
  const document = await getDoc(
    doc(db, PLAYERS_COLLECTION, id).withConverter(playerConverter),
  );
  const player = document.data()!;
  return player;
};

export const getPlayerByFirebaseUuid = async (id: string): Promise<Player> => {
  const db = getFirestore();
  const document = await getDocs(
    query(
      collection(db, PLAYERS_COLLECTION).withConverter(playerConverter),
      where("firebaseUuid", "==", id),
    ),
  );
  const player = document.docs[0].data()!;
  return player;
};

export const playerConverter: FirestoreDataConverter<Player> = {
  toFirestore: (player) => {
    return {
      name: player.name,
      firebaseUuid: player.firebaseUuid,
      domain: player.domain,
    };
  },
  fromFirestore: (snapshot, options) => {
    const data = snapshot.data(options);
    return <Player>{
      id: snapshot.id,
      firebaseUuid: data["firebaseUuid"],
      name: data["name"],
      // Migration-wise: Provide default if value is undefined
      domain: data["domain"] ?? "unknown",
    };
  },
};
