import { Game, GameSettings, PlayerData } from "@/models/game";
import { DartsScore } from "@/models/score";
import {
  dartScoreFromFirestoreAbbr,
  scoreFirestoreAbbr,
} from "@/utils/dartsScore";
import {
  fromFirestoreTimestamp,
  toFirestoreTimestamp,
} from "@/utils/firestore";
import assert from "assert";
import {
  collection,
  doc,
  getDoc,
  getFirestore,
  runTransaction,
  setDoc,
  updateDoc,
} from "firebase/firestore";
import { DateTime } from "luxon";
import { v4 } from "uuid";

export const GAMES_COLLECTION = "games";

export async function createNewGame() {
  return createGame(undefined, []);
}

export async function createRematch(gameSettings: GameSettings | undefined, playerIds: string[]) {
  return createGame(gameSettings, playerIds);
}

async function createGame(gameSettings: GameSettings | undefined, playerIds: string[]): Promise<string> {
  const db = getFirestore();
  const id = v4();
  const docRef = doc(
    collection(db, GAMES_COLLECTION).withConverter(gameConverter),
    id
  );
  const game: Game = {
    id,
    gameSettings: gameSettings ?? {
      score: 301,
      checkOut: "STRAIGHT_OUT",
    },
    playerIds: playerIds,
    players: Object.fromEntries(
      playerIds.map((id, idx) => [
        id,
        { playerId: id, position: idx, darts: [] },
      ])
    ),
    state: "CREATED",
    createdAt: DateTime.now(),
  };
  await setDoc(docRef, game);
  assert(id === docRef.id);
  return docRef.id;
}

export async function getGame(gameId: string): Promise<Game> {
  const db = getFirestore();
  const ref = doc(db, GAMES_COLLECTION, gameId).withConverter(gameConverter);
  const querySnapshot = await getDoc(ref);
  const game = querySnapshot.data()!;
  return game;
}

export async function resetPlayers(gameId: string, playerIds: string[]) {
  const db = getFirestore();
  await updateDoc(doc(db, GAMES_COLLECTION, gameId), {
    playerIds: playerIds,
    players: Object.fromEntries(
      playerIds.map((id, idx) => [
        id,
        { playerId: id, position: idx, darts: [] },
      ])
    ),
  });
}

export async function setGameSettings(
  gameId: string,
  gameSettings: GameSettings
): Promise<void> {
  const db = getFirestore();
  await updateDoc(doc(db, GAMES_COLLECTION, gameId), {
    gameSettings: gameSettings,
  });
}

export async function startGame(gameId: string): Promise<void> {
  const db = getFirestore();
  // TODO can reset finished or aborted games. It's probably a feature.
  await updateDoc(doc(db, GAMES_COLLECTION, gameId), {
    state: "IN_PROGRESS",
  });
}

export async function finishGame(gameId: string): Promise<void> {
  const db = getFirestore();
  await updateDoc(doc(db, GAMES_COLLECTION, gameId), {
    state: "FINISHED",
  });
}

export async function setDarts(
  gameId: string,
  playerId: string,
  darts: (DartsScore | null)[]
): Promise<void> {
  const db = getFirestore();
  const turnAbbreviations = darts.map((dart) =>
    dart ? scoreFirestoreAbbr(dart) : dart
  );
  const metadata = darts.map((dart) => ({
    ...(dart?.metadata?.setBy && { setBy: dart.metadata?.setBy }),
    ...(dart?.metadata?.setAt && {
      setAt: toFirestoreTimestamp(dart.metadata.setAt),
    }),
  }));
  await runTransaction(db, async (tx) => {
    tx.update(doc(db, GAMES_COLLECTION, gameId), {
      [`players.${playerId}.darts`]: turnAbbreviations,
    });
    tx.update(doc(db, GAMES_COLLECTION, gameId), {
      [`players.${playerId}.dartsMetadata`]: metadata,
    });
  });
}

const playerDataFromFirestore = (data: Record<string, any>): PlayerData => {
  return <PlayerData>{
    playerId: data.playerId,
    position: data.position,
    darts: (data.darts as (string | null)[]).map((dart, index) => {
      const dartsMetadata = data.dartsMetadata?.[index];
      return dart
        ? {
          ...dartScoreFromFirestoreAbbr(dart),
          ...(dartsMetadata && {
            metadata: {
              setAt: fromFirestoreTimestamp(dartsMetadata.setAt),
              setBy: dartsMetadata.setBy,
            } satisfies DartsScore["metadata"],
          }),
        }
        : dart;
    }),
  };
};

export const gameConverter = {
  toFirestore: (game: Game) => {
    return {
      ...game,
      createdAt: toFirestoreTimestamp(game.createdAt),
    };
  },
  fromFirestore: (snapshot: any, options: any) => {
    const data = snapshot.data(options);
    return <Game>{
      ...data,
      id: snapshot.id,
      players: Object.fromEntries(
        Object.entries(data["players"] as Record<string, any>).map(
          ([playerId, playerData]) => [
            playerId,
            playerDataFromFirestore(playerData),
          ]
        )
      ),
      createdAt: fromFirestoreTimestamp(data["createdAt"]),
    };
  },
};
