import { getApp } from "firebase/app";
import { initializeFirestore } from "firebase/firestore";
import { initializeFirebase } from "@/firebaseConfig";
import { FirebaseAuthService } from "./auth";
import { UserService } from "./user";

initializeFirebase();
initializeFirestore(getApp(), { ignoreUndefinedProperties: true });
const firebaseAuthService = new FirebaseAuthService();
export const userService = new UserService(firebaseAuthService);
