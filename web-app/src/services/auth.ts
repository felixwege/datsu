import { AuthUser } from "@/models/player";
import { getApp } from "firebase/app";
import {
  Auth,
  getAuth,
  signInWithEmailAndPassword,
  signOut,
  User as FirebaseAuthUser,
  createUserWithEmailAndPassword,
  sendPasswordResetEmail,
} from "firebase/auth";

export class FirebaseAuthService {
  private readonly auth: Auth;
  private readonly _listeners: Array<(currentUser: AuthUser | null) => void> =
    [];
  private _currentUser: AuthUser | null | undefined;

  constructor() {
    this.auth = getAuth(getApp());
    this._currentUser = undefined;
    this.auth.onAuthStateChanged((user) => {
      const currentUser = user ? authUserFromFirebaseAuthUser(user) : null;
      this._currentUser = currentUser;
      for (const listener of this._listeners) {
        try {
          listener(currentUser);
        } catch (e) {
          console.error(e);
        }
      }
    });
  }

  get currentUser() {
    return this._currentUser;
  }

  /** Returns a function which unregisters the given listener again. */
  onAuthStateChanged(
    listener: (currentUser: AuthUser | null) => void,
  ): () => void {
    this._listeners.push(listener);
    return () => {
      const i = this._listeners.indexOf(listener);
      if (i >= 0) {
        this._listeners.splice(i, 1);
      }
    };
  }

  async signInWithUserNameAndPassword(
    username: string,
    password: string,
  ): Promise<AuthUser> {
    const credential = await signInWithEmailAndPassword(
      this.auth,
      username,
      password,
    );
    if (!credential.user) {
      throw new Error(`Could not log in with username ${username}`);
    }
    return authUserFromFirebaseAuthUser(credential.user);
  }

  async registerWithUserNameAndPassword(
    email: string,
    password: string,
  ): Promise<AuthUser> {
    const credential = await createUserWithEmailAndPassword(
      this.auth,
      email,
      password,
    );
    if (!credential.user) {
      throw new Error(`Could not register ${email}`);
    }
    return authUserFromFirebaseAuthUser(credential.user);
  }

  async resetPassword(email: string): Promise<void> {
    return await sendPasswordResetEmail(this.auth, email);
  }

  logout(): Promise<void> {
    return signOut(this.auth);
  }

  getIdToken(): Promise<string> | undefined {
    return this.auth.currentUser?.getIdToken();
  }
}

const authUserFromFirebaseAuthUser = (user: FirebaseAuthUser): AuthUser => ({
  id: user.uid,
  displayName: user.displayName ?? undefined,
  avatarUrl: user.photoURL ?? undefined,
  email: user.email ?? undefined,
});
