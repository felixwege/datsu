import { Player, User } from "@/models/player";
import { FirebaseAuthService } from "./auth";
import { getPlayerByFirebaseUuid, playerConverter } from "./player";
import {
  addDoc,
  collection,
  doc,
  getDoc,
  getFirestore,
  setDoc,
} from "firebase/firestore";

const PLAYER_COLLECTION = "players";

export class UserService {
  private _currentUser: User | null | undefined;
  private readonly _listeners: Array<(currentUser: User | null) => void> = [];

  constructor(private firebaseAuthService: FirebaseAuthService) {
    this.firebaseAuthService.onAuthStateChanged(async (authUser) => {
      if (!authUser) {
        this._currentUser = null;
        this.updateListeners(null);
        return;
      }

      if (this._currentUser?.authUser.id === authUser.id) {
        return;
      }

      const player = await getPlayerByFirebaseUuid(authUser.id);
      if (!player) {
        throw new Error(`Player for authUser ${authUser.id} not found`);
      }

      const currentUser = { authUser, player };
      this._currentUser = currentUser;
      this.updateListeners(currentUser);
    });
  }

  private updateListeners(currentUser: User | null) {
    for (const listener of this._listeners) {
      try {
        listener(currentUser);
      } catch (e) {
        console.error(e);
      }
    }
  }

  get currentUser() {
    return this._currentUser;
  }

  /** Returns a function which unregisters the given listener again. */
  onAuthStateChanged(listener: (currentUser: User | null) => void): () => void {
    this._listeners.push(listener);
    return () => {
      const i = this._listeners.indexOf(listener);
      if (i >= 0) {
        this._listeners.splice(i, 1);
      }
    };
  }

  async login(username: string, password: string) {
    const authUser =
      await this.firebaseAuthService.signInWithUserNameAndPassword(
        username,
        password,
      );
    const player = await getPlayerByFirebaseUuid(authUser.id);
    if (!player) {
      throw new Error(`Player for authUser ${authUser.id} not found`);
    }

    this._currentUser = { authUser, player };
  }

  async register(username: string, email: string, password: string) {
    const authUser =
      await this.firebaseAuthService.registerWithUserNameAndPassword(
        email,
        password,
      );
    console.log("authUser", authUser);
    const db = getFirestore();
    const emailDomain = email.split("@")[1]
    const newPlayer = <Player>{
      firebaseUuid: authUser.id,
      name: username,
      domain: emailDomain
    };
    const playerRef = await addDoc(
      collection(db, PLAYER_COLLECTION).withConverter(playerConverter),
      newPlayer,
    );

    const player = await getDoc(playerRef);
    if (!player || !player.data()) {
      throw new Error(`Registration failed`);
    }

    this._currentUser = { authUser, player: player.data()! };
  }

  async resetPassword(email: string) {
    return await this.firebaseAuthService.resetPassword(email);
  }

  async logout() {
    await this.firebaseAuthService.logout();
  }
}
