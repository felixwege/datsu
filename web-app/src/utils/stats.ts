import { Game, STATS_CHECKOUT } from "@/models/game";
import { PlayerStats } from "@/models/stats";
import { containsBust, isBreakfast, scoreAbbr, scoreFromDarts, turnsFromDarts } from "./dartsScore";
import { ALL_POSSIBLE_SCORES } from "@/models/score";
import { isDefined } from "./filter";

export function buildPlayerStats(
  playerId: string,
  games: Game[],
): { [checkout: string]: PlayerStats } {
  const stats = Object.fromEntries(
    STATS_CHECKOUT.map((checkout) => [
      checkout,
      buildPlayerCheckoutStats(
        playerId,
        checkout === "COMBINED"
          ? games
          : games.filter((game) => game.gameSettings.checkOut === checkout),
      ),
    ]),
  );
  return stats;
}

export function buildPlayerCheckoutStats(
  playerId: string,
  games: Game[],
): PlayerStats {
  if (!games) {
    return getDefaultStats(playerId);
  }

  const allGamesDarts = games.map((game) => game.players[playerId].darts);
  const allConsecutiveDarts = allGamesDarts.flat();
  const allDarts = allConsecutiveDarts.filter(isDefined);
  const numberOfDarts = allDarts.length;
  const counts = Object.fromEntries(ALL_POSSIBLE_SCORES.map((it) => [it, 0]));
  allDarts.map(scoreAbbr).forEach((it) => counts[it]++);
  const zeroCount = allDarts.filter((it) => it.value === 0).length;
  const doubleRatio =
    allDarts.filter((it) => it.modifier === "double").length / numberOfDarts;
  const tripleRatio =
    allDarts.filter((it) => it.modifier === "triple").length / numberOfDarts;
  const firstNineAverages = allGamesDarts
    .map((gameDarts) => {
      const upToNineDarts = gameDarts.slice(0, 9);
      const turnCount = turnsFromDarts(upToNineDarts).length;
      if (!turnCount) {
        return null;
      }

      return scoreFromDarts(upToNineDarts) / turnCount;
    })
    .filter(isDefined);
  const firstNineAverage =
    firstNineAverages.reduce((acc, average) => acc + average, 0) /
    firstNineAverages.length;

  // Max score is the highest score in a single turn
  const maxScore = Math.max(
    ...allGamesDarts.map((gameDarts) => {
      const gameTurns = turnsFromDarts(gameDarts);
      const gameTurnsScores = gameTurns.map(scoreFromDarts);
      return Math.max(...gameTurnsScores);
    }),
  );

  const turnScores = allGamesDarts.flatMap((gameDarts) => {
    const gameTurns = turnsFromDarts(gameDarts);
    const gameTurnsScores = gameTurns.map(scoreFromDarts);
    return gameTurnsScores;
  });
  const average =
    turnScores.reduce((acc, sum) => acc + sum, 0) / turnScores.length;

  const allGamesTurnScores = allGamesDarts.flatMap((gameDarts) => {
    const gameTurns = turnsFromDarts(gameDarts);
    const gameTurnsScores = gameTurns.map(scoreFromDarts);
    return gameTurnsScores;
  });
  const sixtyPlusCount = allGamesTurnScores.filter((it) => it >= 60).length;
  const hundredPlusCount = allGamesTurnScores.filter((it) => it >= 100).length;
  const hundredFourtyPlusCount = allGamesTurnScores.filter(
    (it) => it >= 140,
  ).length;
  const hundredEightyCount = allGamesTurnScores.filter(
    (it) => it === 180,
  ).length;

  const reducedStats = games.reduce((acc, game) => {
    const playerData = game.players[playerId];
    const turns = turnsFromDarts(playerData.darts);
    const isWon =
      game.gameSettings.score - scoreFromDarts(playerData.darts) === 0;
    const gameBustCount = turns.reduce(
      (bustCount, turn) => (containsBust(turn) ? bustCount + 1 : bustCount),
      0,
    );
    const gameBreakfastCount = turns.reduce(
      (breakfastCount, turn) =>
        isBreakfast(turn) ? breakfastCount + 1 : breakfastCount,
      0,
    );

    return {
      ...acc,
      tripleTwentyCount: (acc.tripleTwentyCount += playerData.darts
        .filter(isDefined)
        .filter((it) => it?.value === 20 && it.modifier === "triple").length),
      tripleNineteenCount: (acc.tripleNineteenCount += playerData.darts
        .filter(isDefined)
        .filter((it) => it?.value === 19 && it.modifier === "triple").length),
      bustCount: (acc.bustCount += gameBustCount),
      breakfastCount: (acc.breakfastCount += gameBreakfastCount),
      numberOfWins: (acc.numberOfWins += isWon ? 1 : 0),
    };
  }, getDefaultStats(playerId));

  return {
    ...reducedStats,
    // TODO: only count finished games
    numberOfGames: games.length,
    numberOfDarts,
    tripleTwentyRatio: reducedStats.tripleTwentyCount / numberOfDarts,
    tripleNineteenRatio: reducedStats.tripleNineteenCount / numberOfDarts,
    zeroCount,
    doubleRatio,
    tripleRatio,
    firstNineAverage,
    average,
    maxScore,
    sixtyPlusCount,
    hundredPlusCount,
    hundredFourtyPlusCount,
    hundredEightyCount,
    counts,
  };
}

const getDefaultStats = (playerId: string): PlayerStats => ({
  playerId,
  numberOfGames: 0,
  numberOfDarts: 0,
  bustCount: 0,
  average: 0,
  tripleTwentyCount: 0,
  tripleTwentyRatio: 0,
  tripleNineteenCount: 0,
  tripleNineteenRatio: 0,
  zeroCount: 0,
  doubleRatio: 0,
  tripleRatio: 0,
  breakfastCount: 0,
  numberOfWins: 0,
  firstNineAverage: 0,
  maxScore: 0,
  sixtyPlusCount: 0,
  hundredPlusCount: 0,
  hundredFourtyPlusCount: 0,
  hundredEightyCount: 0,
  counts: {},
});
