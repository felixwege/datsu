import { CheckOut } from "@/models/game";

export function formatCheckOut(
  checkOut: CheckOut,
  opt: { short?: boolean } = { short: false }
): string {
  switch (checkOut) {
    case "STRAIGHT_OUT":
      return opt.short ? "SO" : "Straight Out";
    case "DOUBLE_OUT":
      return opt.short ? "DO" : "Double Out";
    case "MASTER_OUT":
      return opt.short ? "MO" : "Master Out";
  }
}
