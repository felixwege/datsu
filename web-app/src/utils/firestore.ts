import { Timestamp } from "firebase/firestore";
import { DateTime } from "luxon";

export function fromFirestoreTimestamp(timestamp: Timestamp): DateTime;
export function fromFirestoreTimestamp(
  timestamp?: Timestamp
): DateTime | undefined {
  if (!timestamp) {
    return undefined;
  }

  return DateTime.fromJSDate(timestamp.toDate());
}

export function toFirestoreTimestamp(dateTime?: DateTime): Timestamp | null {
  if (!dateTime) {
    return null;
  }

  return Timestamp.fromDate(dateTime.toJSDate());
}

export function toFirestoreTimestampFromIsoString(
  isoString?: string
): Timestamp | null {
  if (!isoString) {
    return null;
  }

  const dateTime = DateTime.fromISO(isoString);
  return toFirestoreTimestamp(dateTime);
}
