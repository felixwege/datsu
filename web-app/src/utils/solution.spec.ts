import { expect, test } from "vitest"
import { SOLUTIONS } from "./solution"
import { scoreFromDarts } from "./dartsScore"
import { isDefined } from "./filter"

test.each(Array.from({ length: 180 }, (_, i) => [i + 1]))("test straight out solution %i", (score) => {
  const solution = SOLUTIONS["STRAIGHT_OUT"][score];
  if (solution === null) {
    return;
  }

  const darts = solution.filter(isDefined);
  expect(scoreFromDarts(darts)).toBe(score);
});

test.each(Array.from({ length: 180 }, (_, i) => [i + 1]))("test double out solution %i", (score) => {
  const solution = SOLUTIONS["DOUBLE_OUT"][score];
  if (solution === null) {
    return;
  }

  const darts = solution.filter(isDefined);
  expect(scoreFromDarts(darts)).toBe(score);
  expect(darts[darts.length - 1].modifier).toBe("double")
});

test.each(Array.from({ length: 180 }, (_, i) => [i + 1]))("test master out solution %i", (score) => {
  const solution = SOLUTIONS["MASTER_OUT"][score];
  if (solution === null) {
    return;
  }

  const darts = solution.filter(isDefined);
  expect(scoreFromDarts(darts)).toBe(score);
  expect(darts[darts.length - 1].modifier).toSatisfy((modifier) => modifier === "double" || modifier === "triple")
});
