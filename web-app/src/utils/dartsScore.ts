import { GameSettings, PlayerData } from "@/models/game";
import { DartsScore, DartsScoreValue } from "@/models/score";
import { isDefined } from "./filter";

export function scoreFirestoreAbbr(score: DartsScore) {
  const bustMarker = score.isBust ? "X" : "";
  return score.modifier === "triple"
    ? `T${score.value}${bustMarker}`
    : score.modifier === "double"
    ? `D${score.value}${bustMarker}`
    : `${score.value}${bustMarker}`;
}

export function scoreAbbr(score: DartsScore) {
  return score.modifier === "triple"
    ? `T${score.value}`
    : score.modifier === "double"
    ? `D${score.value}`
    : `${score.value}`;
}

export function dartScoreFromFirestoreAbbr(str: string): DartsScore {
  const modifier =
    str[0] === "T" ? "triple" : str[0] === "D" ? "double" : "single";
  const isBust = str.slice(-1, str.length) === "X";
  const value = parseInt(
    str.substring(modifier !== "single" ? 1 : 0),
    10
  ) as DartsScoreValue;
  return {
    modifier,
    value,
    isBust,
  };
}

export function computeScore(score: DartsScore): number {
  const modifier =
    score.modifier === "triple" ? 3 : score.modifier === "double" ? 2 : 1;
  return modifier * score.value;
}

export const convertToBust = (score: DartsScore | null): DartsScore | null => {
  return score ? { ...score, isBust: true } : null;
};

export const revertBust = (score: DartsScore | null): DartsScore | null => {
  return score ? { ...score, isBust: false } : null;
};

export const scoreFromDarts = (darts: (DartsScore | null)[]) => {
  return darts.reduce((acc, turn) => {
    const value = scoreValueFromDartsScore(turn);
    return acc + value;
  }, 0);
};

// Use this for calculating a player's score
export const scoreValueFromDartsScore = (score: DartsScore | null) => {
  if (!score || score.isBust) return 0;
  return calcValue(score);
};

// This is for displaying a score value (and the reason for returning a string value)
// DO NOT use this for calculating a player's score
export const displayValueFromDartsScore = (
  score: DartsScore | null
): string => {
  if (!score) return "X";
  return scoreAbbr(score);
};

const calcValue = (score: DartsScore) => {
  return (
    score.value *
    (score.modifier === "triple" ? 3 : score.modifier === "double" ? 2 : 1)
  );
};

export const isBust = (
  score: DartsScore,
  currentPlayerRemainingScore: number,
  gameSettings: GameSettings
) => {
  const isDownToZero = currentPlayerRemainingScore - scoreValueFromDartsScore(score) === 0;
  if (
    isDownToZero &&
    gameSettings.checkOut === "DOUBLE_OUT" &&
    score.modifier !== "double"
  ) {
    return true;
  }

  if (
    isDownToZero &&
    gameSettings.checkOut === "MASTER_OUT" &&
    score.modifier === "single"
  ) {
    return true;
  }

  const isDownToOne = currentPlayerRemainingScore - scoreValueFromDartsScore(score) === 1;
  const isDoubleOrMasterOut = gameSettings.checkOut === "DOUBLE_OUT" || gameSettings.checkOut === "MASTER_OUT";
  if (isDownToOne && isDoubleOrMasterOut) {
    return true;
  }

  return scoreValueFromDartsScore(score) > currentPlayerRemainingScore;
};

export function getScoresToWrite(
  currentPlayerData: PlayerData,
  currentPlayerRemainingScore: number,
  score: DartsScore,
  gameSettings: GameSettings
): (DartsScore | null)[] {
  const currentDarts = currentPlayerData.darts;
  if (!isBust(score, currentPlayerRemainingScore, gameSettings)) {
    return [...currentDarts, score];
  }

  const bustScore = convertToBust(score);
  const turnCount = currentPlayerData.darts.length + 1;

  switch (turnCount % 3) {
    case 0:
      return [
        ...currentDarts.slice(0, -2),
        ...currentDarts.slice(-2).map(convertToBust),
        bustScore,
      ];
    case 1:
      return [...currentDarts, bustScore, null, null];
    case 2:
      return [
        ...currentDarts.slice(0, -1),
        ...currentDarts.slice(-1).map(convertToBust),
        bustScore,
        null,
      ];
    default:
      return currentDarts;
  }
}

export function getScoresToUndo(playerData: PlayerData): (DartsScore | null)[] {
  const currentDarts = playerData.darts;
  const turnCount = playerData.darts.length;

  switch (turnCount % 3) {
    case 0: {
      const dartsCount = currentDarts.slice(-3).filter(isDefined).length;
      const notCarriedOutDarts = 3 - dartsCount;
      return [
        ...currentDarts.slice(0, -3),
        ...currentDarts
          .slice(currentDarts.length - 3, -(notCarriedOutDarts + 1))
          .map(revertBust),
      ];
    }
    case 1:
      return [
        ...currentDarts.slice(0, -1),
        ...currentDarts.slice(currentDarts.length - 1, -1).map(revertBust),
      ];
    case 2:
      return [
        ...currentDarts.slice(0, -2),
        ...currentDarts.slice(currentDarts.length - 2, -1).map(revertBust),
      ];
    default:
      return currentDarts;
  }
}

export function turnsFromDarts(darts: (DartsScore | null)[]) {
  const turns: (DartsScore | null)[][] = [];
  const turnLength = 3;
  for (let i = 0; i < darts.length; i += turnLength) {
    const turnDarts = darts.slice(i, i + turnLength);
    turns.push(turnDarts);
  }
  return turns;
}

export function containsBust(darts: (DartsScore | null)[]): boolean {
  return darts.some((dart) => dart?.isBust);
}

export function isBreakfast(turn: (DartsScore | null)[]): boolean {
  const validDarts = turn.filter(isDefined);
  if (validDarts.length !== 3) {
    return false;
  }

  const abbr = validDarts.map((it) => scoreAbbr(it));
  return abbr.includes("5") && abbr.includes("20") && abbr.includes("1");
}
