import { expect, test } from "vitest"
import { isBust } from "./dartsScore"
import { DartsScore } from "@/models/score"
import { GameSettings } from "@/models/game"

interface TestData {
  description: string,
  score: DartsScore,
  currentPlayerRemainingScore: number,
  gameSettings: GameSettings,
  expected: boolean,
}

const testData: TestData[] = [
  {
    description: "60 - T20, straight out -> no bust",
    score: {
      value: 20,
      modifier: "triple",
      isBust: false,
    },
    currentPlayerRemainingScore: 60,
    gameSettings: {
      score: 301,
      checkOut: "STRAIGHT_OUT",
    },
    expected: false,
  },
  {
    description: "59 - T20, straight out -> bust",
    score: {
      value: 20,
      modifier: "triple",
      isBust: false,
    },
    currentPlayerRemainingScore: 59,
    gameSettings: {
      score: 301,
      checkOut: "STRAIGHT_OUT",
    },
    expected: true,
  },
  {
    description: "3 - T1, double out -> bust",
    score: {
      value: 1,
      modifier: "triple",
      isBust: false,
    },
    currentPlayerRemainingScore: 3,
    gameSettings: {
      score: 301,
      checkOut: "DOUBLE_OUT",
    },
    expected: true,
  },
  {
    description: "3 - T1, master out -> no bust",
    score: {
      value: 1,
      modifier: "triple",
      isBust: false,
    },
    currentPlayerRemainingScore: 3,
    gameSettings: {
      score: 301,
      checkOut: "MASTER_OUT",
    },
    expected: false,
  },
  {
    description: "2 - D1, double out -> no bust",
    score: {
      value: 1,
      modifier: "double",
      isBust: false,
    },
    currentPlayerRemainingScore: 2,
    gameSettings: {
      score: 301,
      checkOut: "DOUBLE_OUT",
    },
    expected: false,
  },
  {
    description: "2 - D1, master out -> no bust",
    score: {
      value: 1,
      modifier: "double",
      isBust: false,
    },
    currentPlayerRemainingScore: 2,
    gameSettings: {
      score: 301,
      checkOut: "MASTER_OUT",
    },
    expected: false,
  },
  {
    description: "2 - 1, straight out -> no bust",
    score: {
      value: 1,
      modifier: "single",
      isBust: false,
    },
    currentPlayerRemainingScore: 2,
    gameSettings: {
      score: 301,
      checkOut: "STRAIGHT_OUT",
    },
    expected: false,
  },
  {
    description: "2 - 1, double out -> bust",
    score: {
      value: 1,
      modifier: "single",
      isBust: false,
    },
    currentPlayerRemainingScore: 2,
    gameSettings: {
      score: 301,
      checkOut: "DOUBLE_OUT",
    },
    expected: true,
  },
  {
    description: "2 - 1, master out -> bust",
    score: {
      value: 1,
      modifier: "single",
      isBust: false,
    },
    currentPlayerRemainingScore: 2,
    gameSettings: {
      score: 301,
      checkOut: "MASTER_OUT",
    },
    expected: true,
  },
]

test.each(testData)("test isBust %s", ({ description, score, currentPlayerRemainingScore, gameSettings, expected }: TestData) => {
  expect(isBust(score, currentPlayerRemainingScore, gameSettings)).toBe(expected);
});
