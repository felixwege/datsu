"use client";

import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { AppProps } from "next/app";
import { RecoilRoot } from "recoil";
import { RecoilURLSyncJSON } from "recoil-sync";
import { useEffect, useState } from "react";
import { ChakraProvider } from "@chakra-ui/react";
import "../services/services";
import Head from "next/head";
import { SnackbarProvider } from "notistack";

const queryClient = new QueryClient();

export type Children = {
  readonly children?: React.ReactNode;
};
// This is a hack so we don't get hydration errors for the url sync
// If we ever need to do SSR we have to remove this (or only use it on certain pages)
const DisableSSR = (props: Children) => {
  const [isMounted, setIsMounted] = useState(false);
  useEffect(() => setIsMounted(true), []);
  return isMounted ? <div>{props.children}</div> : null;
};

export default function WebApp(props: AppProps) {
  const { Component, pageProps } = props;
  return (
    <RecoilRoot>
      <QueryClientProvider client={queryClient}>
        <DisableSSR>
          <RecoilURLSyncJSON location={{ part: "queryParams" }}>
            <SnackbarProvider>
              <Head>
                <meta charSet="UTF-8" />
                <meta name="description" content="Datsu" />
                <meta name="keywords" content="Awesome Darts App" />
                <link rel="manifest" href="/manifest.json" />
                <title>Datsu</title>
              </Head>
              <ChakraProvider>
                <Component {...pageProps}></Component>
              </ChakraProvider>
            </SnackbarProvider>
          </RecoilURLSyncJSON>
        </DisableSSR>
      </QueryClientProvider>
    </RecoilRoot>
  );
}
