
import { createIcon } from "@chakra-ui/react";

export const AddIcon = createIcon({
  displayName: "AddIcon",
  viewBox: "0 -960 960 960",
  d: "M440-440H200v-80h240v-240h80v240h240v80H520v240h-80v-240Z",
});