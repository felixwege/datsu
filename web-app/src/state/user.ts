import { atom } from "recoil";
import { userService } from "../services/services";
import { User } from "@/models/player";

export const userState = atom<User | null>({
  key: "authUser",
  default: new Promise(() => { }),
  effects: [
    ({ setSelf }) => {
      const currentUser = userService.currentUser;

      if (currentUser) {
        setSelf(currentUser);
      }

      return userService.onAuthStateChanged((user) => {
        setSelf(user);
      });
    },
  ],
});
