import { Center, Flex, Spinner } from "@chakra-ui/react";

export function LoadingSpinner() {
  return (
    <Flex
      width={"100%"}
      height={"100dvh"}
      alignContent={"center"}
      justifyContent={"center"}
    >
      <Center>
        <Spinner />
      </Center>
    </Flex>
  );
}
