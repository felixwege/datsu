import { Player } from "@/models/player";
import {
  Box,
  Button,
  Checkbox,
  Divider,
  HStack,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  VStack,
} from "@chakra-ui/react";
import { useState } from "react";

export interface PlayerSelectModalProps {
  isModalOpen: boolean;
  initialCheckedPlayerIds?: string[];
  playerEntries: { [playerId: string]: Player };
  onConfirm: (selectedPlayerIds: string[]) => void;
  close: () => void;
  userDomain: string;
}

export function PlayerSelectModal({
  isModalOpen,
  initialCheckedPlayerIds,
  playerEntries,
  onConfirm,
  close,
  userDomain
}: PlayerSelectModalProps) {
  const [checkedPlayerIds, setCheckedPlayerIds] = useState<string[]>(
    initialCheckedPlayerIds ?? []
  );

  const handleTogglePlayer = (playerId: string, checked: boolean) => {
    if (checked) {
      const playerSet = new Set([...checkedPlayerIds, playerId]);
      setCheckedPlayerIds([...playerSet]);
      return;
    }

    const newCheckedPlayerIds = [...checkedPlayerIds];
    const index = newCheckedPlayerIds.indexOf(playerId);
    newCheckedPlayerIds.splice(index, 1);
    setCheckedPlayerIds(newCheckedPlayerIds);
    return;
  };

  const playerByName = Object.values(playerEntries).sort((a, b) => a.name.localeCompare(b.name));
  const domainPlayers = playerByName.filter((p) => p.domain === userDomain);
  const foreignPlayers = playerByName.filter((p) => p.domain !== userDomain)

  return (
    <Modal isOpen={isModalOpen} onClose={close}>
      <ModalOverlay />
      <ModalContent>
        <ModalHeader>Add Players</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          {domainPlayers && (
              <VStack>
                {domainPlayers.map((player) => (
                    <HStack key={player.id}>
                      <Checkbox
                          isChecked={checkedPlayerIds.includes(player.id)}
                          onChange={(e) => {
                            handleTogglePlayer(player.id, e.target.checked);
                          }}
                      ></Checkbox>
                      <Box>{player.name}</Box>
                    </HStack>
                ))}
              </VStack>
          )}
          {domainPlayers && foreignPlayers && <Divider />}
          {foreignPlayers && (
            <VStack>
              {foreignPlayers.map((player) => (
                <HStack key={player.id}>
                  <Checkbox
                    isChecked={checkedPlayerIds.includes(player.id)}
                    onChange={(e) => {
                      handleTogglePlayer(player.id, e.target.checked);
                    }}
                  ></Checkbox>
                  <Box>{player.name}</Box>
                </HStack>
              ))}
            </VStack>
          )}
        </ModalBody>
        <ModalFooter>
          <Button
            onClick={() => {
              onConfirm(checkedPlayerIds);
              close();
            }}
          >
            Save
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  );
}
