import { Box, Heading, VStack } from "@chakra-ui/react";
import { useCurrentGamesWithPlayers } from "@/hooks/game";
import { GameButton } from "@/components/gameButton";
import { LoginGuard } from "@/components/loginGuard";

export default function SpectatorPage() {
  return (
    <LoginGuard>
      <VStack>
        <CurrentGames />
      </VStack>
    </LoginGuard>
  );
}

export function CurrentGames() {
  const currentGames = useCurrentGamesWithPlayers();

  return (
    <Box>
      <VStack>
        {currentGames && currentGames.length && (
          <Heading size="sm">Unfinished games</Heading>
        )}
        {currentGames &&
          currentGames.map((game) => <GameButton key={game.id} game={game} />)}
      </VStack>
    </Box>
  );
}
