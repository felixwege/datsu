import { LoginGuard } from "@/components/loginGuard";
import { PlayerSelectModal } from "@/components/playerSelectModal";
import { usePlayersStats } from "@/hooks/game";
import { useAllPlayers } from "@/hooks/players";
import { useUser } from "@/hooks/user";
import { StatsCheckout, STATS_CHECKOUT } from "@/models/game";
import {
  Text,
  Box,
  Flex,
  HStack,
  Table,
  TableContainer,
  Tbody,
  Td,
  Thead,
  Tr,
  VStack,
  useRadioGroup,
  useRadio,
  Button,
} from "@chakra-ui/react";
import { DateTime } from "luxon";
import { useState } from "react";

export default function StatsPage() {
  return (
    <LoginGuard>
      <Stats />
    </LoginGuard>
  );
}

export function Stats() {
  const user = useUser();
  const [playerIds, setPlayerIds] = useState<string[]>([user.player.id]);
  const [isOpen, setIsOpen] = useState(false);
  const { data: players } = useAllPlayers();

  const timeSpanOptions = ["last week", "last month", "all time"];
  const [since, setSince] = useState<DateTime | undefined>(
    DateTime.now().minus({ month: 1 }),
  );

  const checkoutOptions = ["straight out", "double out", "overall"];
  const [checkout, setCheckout] = useState<StatsCheckout>("COMBINED");

  const {
    getRootProps: getTimeSpanRootProps,
    getRadioProps: getTimeSpanRadioProps,
  } = useRadioGroup({
    name: "timeSpan",
    defaultValue: "last month",
    onChange: (nextValue) => {
      setSince(
        nextValue === "last week"
          ? DateTime.now().minus({ week: 1 })
          : nextValue === "last month"
            ? DateTime.now().minus({ month: 1 })
            : undefined,
      );
    },
  });

  const {
    getRootProps: getCheckoutRootProps,
    getRadioProps: getCheckoutRadioProps,
  } = useRadioGroup({
    name: "checkOut",
    defaultValue: "overall",
    onChange: (nextValue) => {
      setCheckout(
        nextValue === "straight out"
          ? "STRAIGHT_OUT"
          : nextValue === "double out"
            ? "DOUBLE_OUT"
            : "COMBINED",
      );
    },
  });

  const timeSpanGroup = getTimeSpanRootProps();
  const checkoutGroup = getCheckoutRootProps();

  return (
    <VStack>
      <HStack {...checkoutGroup}>
        {checkoutOptions.map((value) => {
          const radio = getCheckoutRadioProps({ value });
          return <RadioButton key={value} label={value} {...radio} />;
        })}
      </HStack>
      <HStack {...timeSpanGroup}>
        {timeSpanOptions.map((value) => {
          const radio = getTimeSpanRadioProps({ value });
          return <RadioButton key={value} label={value} {...radio} />;
        })}
      </HStack>
      <Button onClick={() => setIsOpen(true)}>Add</Button>
      <StatsTable playerIds={playerIds} checkout={checkout} since={since} />
      {players && (
        <PlayerSelectModal
          isModalOpen={isOpen}
          initialCheckedPlayerIds={playerIds}
          playerEntries={players}
          onConfirm={(ids) => {
            setPlayerIds(ids);
          }}
          close={() => setIsOpen(false)}
          userDomain={user.player.domain}
        />
      )}
    </VStack>
  );
}

export interface StatsTableProps {
  playerIds: string[];
  checkout: StatsCheckout;
  since: DateTime | undefined;
}

export function StatsTable({ playerIds, checkout, since }: StatsTableProps) {
  const allStats = usePlayersStats(playerIds, since);
  const { data: players } = useAllPlayers();
  const checkoutStats = Object.values(allStats).map((stats) => stats[checkout]);

  return (
    <Flex
      justifyContent={"center"}
      fontSize="xs"
      width="100%"
      maxWidth="100%"
      overflowY="scroll"
    >
      {players && (
        <TableContainer width={"100%"}>
          <Table variant="striped" size={"sm"}>
            <Thead>
              <Tr>
                <Td></Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>
                    <Text
                      fontWeight={"bold"}
                      style={{
                        writingMode: "vertical-lr",
                      }}
                    >
                      {players[stats.playerId].name}
                    </Text>
                  </Td>
                ))}
              </Tr>
            </Thead>
            <Tbody>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    Games
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{stats.numberOfGames}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    Won
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{stats.numberOfWins}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    Darts
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{stats.numberOfDarts}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    Busts
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{stats.bustCount}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    Breakfasts
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{stats.breakfastCount}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    Ø
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{`${stats.average.toFixed(2)}`}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    First-9 Ø
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{`${stats.firstNineAverage.toFixed(
                    2,
                  )}`}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    Max score
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{stats.maxScore}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    60+
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{stats.sixtyPlusCount}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    100+
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{stats.hundredPlusCount}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    140+
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{stats.hundredFourtyPlusCount}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    180
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{stats.hundredEightyCount}</Td>
                ))}
              </Tr>

              <Tr>
                <Td colSpan={checkoutStats.length + 1}>
                  <Text pt={"16px"} fontWeight={"bold"}>
                    Ratios
                  </Text>
                </Td>
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    T20 %
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{`${Math.round(
                    stats.tripleTwentyRatio * 100,
                  )}%`}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    T19 %
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{`${Math.round(
                    stats.tripleNineteenRatio * 100,
                  )}%`}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    Double %
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{`${Math.round(
                    stats.doubleRatio * 100,
                  )}%`}</Td>
                ))}
              </Tr>
              <Tr>
                <Td>
                  <Text fontSize={"xs"} fontWeight={"bold"}>
                    Triple %
                  </Text>
                </Td>
                {checkoutStats.map((stats) => (
                  <Td key={stats.playerId}>{`${Math.round(
                    stats.tripleRatio * 100,
                  )}%`}</Td>
                ))}
              </Tr>

              <Tr>
                <Td colSpan={checkoutStats.length + 1}>
                  <Text pt={"16px"} fontWeight={"bold"}>
                    Hits
                  </Text>
                </Td>
              </Tr>
              {checkoutStats?.[0] &&
                Object.entries(checkoutStats[0].counts)
                  .sort(([_, count1], [__, count2]) => count2 - count1)
                  .map(([score, _]) => (
                    <Tr key={score}>
                      <Td>
                        <Text fontSize={"xs"} fontWeight={"bold"}>
                          {score}
                        </Text>
                      </Td>
                      {checkoutStats.map((stats) => (
                        <Td key={stats.playerId}>{stats.counts[score]}</Td>
                      ))}
                    </Tr>
                  ))}
            </Tbody>
          </Table>
        </TableContainer>
      )}
    </Flex>
  );
}

function RadioButton(props: any) {
  const { getInputProps, getRadioProps } = useRadio(props);

  const input = getInputProps();
  const checkbox = getRadioProps();

  return (
    <Box as="label">
      <input {...input} />
      <Box
        {...checkbox}
        borderWidth="1px"
        borderRadius="md"
        boxShadow="md"
        _checked={{
          bg: "gray.200",
        }}
        px={4}
        py={2}
        width={"7.8rem"}
        textAlign={"center"}
      >
        <Text fontWeight={"semibold"}>{props.label}</Text>
      </Box>
    </Box>
  );
}
