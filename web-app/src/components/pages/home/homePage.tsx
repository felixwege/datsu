import { createNewGame } from "@/services/game";
import { Box, Button, HStack, Heading, VStack } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useUser } from "@/hooks/user";
import { userService } from "@/services/services";
import { LoginGuard } from "@/components/loginGuard";
import { useRecentUnfinishedGames } from "@/hooks/game";
import { GameButton } from "@/components/gameButton";

export default function HomePage() {
  const router = useRouter();

  const onCreate = async () => {
    const id = await createNewGame();

    router.push({
      pathname: `/game`,
      query: {
        gameId: id,
      },
    });
  };

  const onOpenStats = async () => {
    router.push({
      pathname: `/stats`,
    });
  };

  return (
    <LoginGuard>
      <VStack>
        <PlayerInfo />
        <RecentGames />
        <HStack>
          <Button onClick={onCreate}>Create</Button>
          <Button variant="outline" onClick={onOpenStats}>
            Stats
          </Button>
          <Button variant="outline" onClick={() => userService.logout()}>
            Logout
          </Button>
        </HStack>
      </VStack>
    </LoginGuard>
  );
}

export function PlayerInfo() {
  const user = useUser();

  return <Heading size="lg">{user.player.name}</Heading>;
}

export function RecentGames() {
  const recentGames = useRecentUnfinishedGames();

  return (
    <Box>
      <VStack>
        {recentGames.length && <Heading size="sm">Unfinished games</Heading>}
        {recentGames.map((game) => (
          <GameButton key={game.id} game={game} />
        ))}
      </VStack>
    </Box>
  );
}
