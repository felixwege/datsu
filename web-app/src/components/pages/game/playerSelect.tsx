import { Player } from "@/models/player";
import { useState } from "react";
import {
  useAvailablePlayers,
  useCurrentGameId,
  useGamePlayers,
} from "@/hooks/game";
import { resetPlayers } from "@/services/game";
import {
  Box,
  Button,
  Checkbox,
  HStack,
  IconButton,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  VStack,
} from "@chakra-ui/react";
import { DeleteIcon } from "@/icons/deleteIcon";
import { AddIcon } from "@/icons/addIcon";
import { PlayerSelectModal } from "@/components/playerSelectModal";
import { useAllPlayers } from "@/hooks/players";
import { useUser } from "@/hooks/user";

export function PlayerSelect() {
  const user = useUser();
  const gameId = useCurrentGameId();
  if (!gameId) {
    throw new Error("No game id");
  }
  const { data: allPlayers } = useAllPlayers();
  const { data: gamePlayers, isLoading } = useGamePlayers();

  const [isModalOpen, setIsDialogOpen] = useState(false);

  const onConfirm = (selectedPlayerIds: string[]) => {
    if (!gamePlayers) return;

    resetPlayers(gameId, selectedPlayerIds);
  };

  return (
    <>
      <VStack>
        <Button onClick={() => setIsDialogOpen(true)}>Add</Button>
        {isLoading && <Box>Loading...</Box>}
        {gamePlayers && (
          <Box>
            {Object.values(gamePlayers).map((player) => (
              <HStack key={player.id}>
                <Box>{player.name}</Box>
              </HStack>
            ))}
          </Box>
        )}
      </VStack>

      {allPlayers && (
        <PlayerSelectModal
          isModalOpen={isModalOpen}
          playerEntries={allPlayers}
          initialCheckedPlayerIds={Object.keys(gamePlayers ?? {})}
          onConfirm={onConfirm}
          close={() => setIsDialogOpen(false)}
          userDomain={user.player.domain}
        />
      )}
    </>
  );
}
