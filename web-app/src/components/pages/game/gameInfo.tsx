import { useCurrentGame } from "@/hooks/game";
import { formatCheckOut } from "@/utils/format";
import { Box, HStack, VStack } from "@chakra-ui/react";

export function GameInfo() {
  const { data: game, isLoading } = useCurrentGame();
  if (isLoading || !game) {
    return <></>;
  }

  return (
    <Box width="100%" maxWidth="384px">
      <VStack>
        <HStack>
          <Box>{game.gameSettings.score}</Box>
          <Box>{formatCheckOut(game.gameSettings.checkOut)}</Box>
        </HStack>
      </VStack>
    </Box>
  );
}
