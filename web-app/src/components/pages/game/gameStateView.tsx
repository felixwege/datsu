import {
  useCurrentPlayerData,
  useGamePlayerDatas,
  useGamePlayers,
  useLastThrowPlayerData,
  usePlayerAverageScore,
  usePlayerLastMove,
  usePlayerNumberOfDarts,
  usePlayerRemainingScore,
} from "@/hooks/game";
import { Player } from "@/models/player";
import { displayValueFromDartsScore } from "@/utils/dartsScore";
import { Box, Flex, VStack, forwardRef } from "@chakra-ui/react";
import { GameInfo } from "./gameInfo";
import { useEffect, useRef } from "react";

export function GameStateView() {
  const gamePlayerDatas = useGamePlayerDatas();
  const { data: players } = useGamePlayers();
  const currentPlayerData = useCurrentPlayerData();
  const currentPlayerRef = useRef<null | HTMLDivElement>(null);

  useEffect(() => {
    currentPlayerRef?.current?.scrollIntoView();
  }, [currentPlayerData]);

  if (!currentPlayerData || !gamePlayerDatas || !players) {
    return <Box>Player data error</Box>;
  }

  return (
    <>
      <GameInfo></GameInfo>
      {gamePlayerDatas.map((playerData, index) => {
        return (
          <PlayerCard
            key={playerData.playerId}
            ref={
              playerData.playerId === currentPlayerData?.playerId
                ? currentPlayerRef
                : null
            }
            player={players[playerData.playerId]}
          />
        );
      })}
    </>
  );
}

interface PlayerCardProps {
  readonly player: Player;
}

const PlayerCard = forwardRef(function MyInput(props: PlayerCardProps, ref) {
  const player = props.player;
  const currentPlayer = useCurrentPlayerData();
  const remainingScore = usePlayerRemainingScore(player.id);
  const averageScore = usePlayerAverageScore(player.id);
  const numberOfDarts = usePlayerNumberOfDarts(player.id);
  const lastDarts = usePlayerLastMove(player.id);
  const lastThrowPlayerData = useLastThrowPlayerData();

  const isCurrentPlayer = currentPlayer?.playerId === player.id;
  const isPlayerWhoDidLastThrow = lastThrowPlayerData?.playerId === player.id;
  const border = isCurrentPlayer ? "2px" : "0px";

  return (
    <Box
      ref={ref}
      width="100%"
      maxWidth="384px"
      padding="8px"
      border={border}
      borderColor="gray.600"
    >
      <Flex height="64px">
        <Box flex="1">
          <VStack>
            <Box>{player.name}</Box>
            {remainingScore && <Box>{remainingScore}</Box>}
          </VStack>
        </Box>
        <Box flex="1">
          <VStack>
            <Box></Box>
            {lastDarts && (!isCurrentPlayer || isPlayerWhoDidLastThrow) && (
              <Box>
                {lastDarts
                  .map((turn) => displayValueFromDartsScore(turn))
                  .join(",")}
              </Box>
            )}
          </VStack>
        </Box>
        <Box flex="1">
          <VStack>
            {numberOfDarts && <Box>{numberOfDarts}</Box>}
            {averageScore && <Box>{averageScore.toFixed(2)}</Box>}
          </VStack>
        </Box>
      </Flex>
    </Box>
  );
});
