import { useEffect, useState } from "react";
import {
  DartsScore,
  DartsScoreModifier,
  DartsScoreValue,
} from "@/models/score";
import { Button, Flex, Text } from "@chakra-ui/react";
import { UndoIcon } from "@/icons/undoIcon";
import { DateTime } from "luxon";
import { useUser } from "@/hooks/user";
import { useCurrentPlayerData } from "@/hooks/game";

const range = (start: number, end: number) => {
  return Array.from(Array(1 + end - start).keys()).map((v) => start + v);
};

export interface DartsKeyboardProps {
  onScore: (score: DartsScore) => void;
  onBack: () => void;
}

export default function DartsKeyboard(props: DartsKeyboardProps) {
  const loggedInPlayerId = useUser().player.id;
  const currentPlayerData = useCurrentPlayerData();
  const [modifier, setModifier] = useState<DartsScoreModifier>("single");
  const [isBlocked, setIsBlocked] = useState(false);

  const lastDartSetAt = currentPlayerData?.darts.slice(-1)[0]?.metadata?.setAt;
  const lastDartSetBy = currentPlayerData?.darts.slice(-1)[0]?.metadata?.setBy;

  useEffect(() => {
    const now = DateTime.now();
    if (
      !isBlocked &&
      lastDartSetAt &&
      lastDartSetBy &&
      now < lastDartSetAt?.plus({ seconds: 5 }) &&
      lastDartSetBy !== loggedInPlayerId
    ) {
      setIsBlocked(true);
      // Set a timer for 5 seconds
      setTimeout(() => {
        setIsBlocked(false);
      }, 5000);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [lastDartSetAt?.toISO(), lastDartSetBy]);

  const onScore = (score: DartsScore) => {
    props.onScore(score);
    // reset to single after each score
    setModifier("single");
  };

  const handleModifier = (newModifier: DartsScoreModifier) => {
    if (newModifier !== modifier) {
      setModifier(newModifier);
    } else {
      setModifier("single");
    }
  };

  return (
    <Flex
      flexDirection="column"
      width="100%"
      maxWidth="384px"
      position="relative"
    >
      {isBlocked && <BlockedOverlay />}
      <Flex>
        {[...range(1, 5)].map((i) => {
          return (
            <DartsKeyboardScoreButton
              key={i}
              value={i as DartsScoreValue}
              modifier={modifier}
              onScore={onScore}
            />
          );
        })}
      </Flex>
      <Flex>
        {[...range(6, 10)].map((i) => {
          return (
            <DartsKeyboardScoreButton
              key={i}
              value={i as DartsScoreValue}
              modifier={modifier}
              onScore={onScore}
            />
          );
        })}
      </Flex>
      <Flex>
        {[...range(11, 15)].map((i) => {
          return (
            <DartsKeyboardScoreButton
              key={i}
              value={i as DartsScoreValue}
              modifier={modifier}
              onScore={onScore}
            />
          );
        })}
      </Flex>
      <Flex>
        {[...range(16, 20)].map((i) => {
          return (
            <DartsKeyboardScoreButton
              key={i}
              value={i as DartsScoreValue}
              modifier={modifier}
              onScore={onScore}
            />
          );
        })}
      </Flex>
      <Flex>
        {[25, 0].map((i) => {
          return (
            <DartsKeyboardScoreButton
              key={i}
              value={i as DartsScoreValue}
              modifier={modifier}
              onScore={onScore}
            />
          );
        })}
        <DartsKeyboardBackButton
          onBack={() => {
            setModifier("single");
            props.onBack();
          }}
        />
      </Flex>
      <Flex>
        <DartsKeyboardModifierButton
          label="Double"
          isSelected={modifier === "double"}
          onClick={() => {
            handleModifier("double");
          }}
        ></DartsKeyboardModifierButton>
        <DartsKeyboardModifierButton
          label="Triple"
          isSelected={modifier === "triple"}
          onClick={() => {
            handleModifier("triple");
          }}
        ></DartsKeyboardModifierButton>
      </Flex>
    </Flex>
  );
}

interface DartsKeyboardScoreButtonProps {
  value: DartsScoreValue;
  modifier: DartsScoreModifier;
  onScore: (score: DartsScore) => void;
}

function DartsKeyboardScoreButton({
  value,
  modifier,
  onScore,
}: DartsKeyboardScoreButtonProps) {
  const loggedInUser = useUser();
  const isDisabled =
    (modifier === "triple" && value === 25) ||
    ((modifier === "double" || modifier === "triple") && value === 0);

  return (
    <Button
      isDisabled={isDisabled}
      flex="1" // TODO make prop
      margin="0px" // TODO make prop
      variant="outline"
      onClick={() => {
        onScore({
          value: value,
          modifier: modifier,
          isBust: false,
          metadata: { setAt: DateTime.now(), setBy: loggedInUser.player.id },
        });
      }}
    >
      {`${value}`}
    </Button>
  );
}

interface DartsKeyboardBackButtonProps {
  onBack: () => void;
}

function DartsKeyboardBackButton(props: DartsKeyboardBackButtonProps) {
  // TODO make prop
  return (
    <Button flex="1" margin="0px" variant="outline" onClick={props.onBack}>
      <UndoIcon></UndoIcon>
    </Button>
  );
}

interface DartsKeyboardModifierButtonProps {
  readonly label: string;
  readonly isSelected: boolean;
  readonly onClick: () => void;
}

function DartsKeyboardModifierButton({
  label,
  isSelected,
  onClick,
}: DartsKeyboardModifierButtonProps) {
  return (
    // TODO make prop
    <Button
      flex="4"
      isActive={isSelected}
      margin="0px"
      variant="outline"
      onClick={onClick}
    >
      {label}
    </Button>
  );
}

const BlockedOverlay = () => {
  return (
    <Flex
      position="absolute"
      justify="center"
      align="center"
      top="0"
      bottom="0"
      left="0"
      right="0"
      backdropFilter="blur(10px)"
      rounded={"8px"}
      bg="rgba(0,0,0,0.4)"
      zIndex={100}
    >
      <Text textAlign="center" color="white">
        Someone else is taking score...
      </Text>
    </Flex>
  );
};
