import QRCode from "react-qr-code";
import { PlayerSelect } from "../playerSelect";
import { GameSettingsSelect } from "../gameSettingsSelect";
import { startGame } from "@/services/game";
import { useCurrentGameId, useGamePlayers } from "@/hooks/game";
import { Box, Button, VStack } from "@chakra-ui/react";

export interface GameCreatedProps { }

export default function GameCreated({ }: GameCreatedProps) {
  const gameId = useCurrentGameId();
  const { data: gamePlayers } = useGamePlayers();
  const onStart = async () => {
    await startGame(gameId);
  };

  const hasPlayers =
    gamePlayers !== undefined && Object.keys(gamePlayers).length !== 0;

  return (
    <VStack>
      <GameSettingsSelect />
      <PlayerSelect />
      {hasPlayers && <Button onClick={onStart}>Start</Button>}
    </VStack>
  );
}
