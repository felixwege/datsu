import { finishGame, setDarts } from "@/services/game";
import DartsKeyboard from "../keyboard";
import { GameStateView } from "../gameStateView";
import {
  displayValueFromDartsScore,
  getScoresToUndo,
  getScoresToWrite,
  scoreFromDarts,
} from "@/utils/dartsScore";
import { GameSettings } from "@/models/game";
import { DartsScore } from "@/models/score";
import {
  useCurrentGame,
  useCurrentPlayerData,
  useLastThrowPlayerData,
  usePlayerLastMove,
  usePlayerRemainingScore,
  useRelativePlayerDataGetter,
} from "@/hooks/game";
import { isDefined } from "@/utils/filter";
import { useState } from "react";
import {
  Box,
  Button,
  Flex,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Spacer,
} from "@chakra-ui/react";
import { LoadingSpinner } from "@/components/loadingSpinner";
import { SOLUTIONS } from "@/utils/solution";

function isFinished(
  gameSettings: GameSettings,
  darts: (DartsScore | null)[]
): boolean {
  return gameSettings.score - scoreFromDarts(darts) === 0;
}

export interface GameInProgressProps {}

export default function GameInProgress({}: GameInProgressProps) {
  const { data: game, isLoading } = useCurrentGame();
  const currentPlayerData = useCurrentPlayerData();
  const relativePlayerDataGetter = useRelativePlayerDataGetter();
  const lastThrowPlayerData = useLastThrowPlayerData();
  const currentPlayerRemainingScore = usePlayerRemainingScore(
    currentPlayerData?.playerId
  );
  const currentPlayerLastDarts = usePlayerLastMove(currentPlayerData?.playerId);
  const [isFinishDialogOpen, setFinishDialogOpen] = useState(false);

  if (isLoading) {
    return <LoadingSpinner />;
  }

  if (!game) {
    return <Box>No game found</Box>;
  }

  if (
    !currentPlayerData ||
    !currentPlayerLastDarts ||
    !lastThrowPlayerData ||
    !relativePlayerDataGetter ||
    !isDefined(currentPlayerRemainingScore)
  ) {
    return <Box>Current player not found</Box>;
  }

  const handleOnScore = async (score: DartsScore) => {
    const scoresToWrite = getScoresToWrite(
      currentPlayerData,
      currentPlayerRemainingScore,
      score,
      game?.gameSettings
    );
    await setDarts(game.id, currentPlayerData.playerId, scoresToWrite);
    if (isFinished(game.gameSettings, scoresToWrite)) {
      setFinishDialogOpen(true);
    }
  };

  const handleOnBack = async () => {
    const playerData =
      lastThrowPlayerData.playerId === currentPlayerData.playerId
        ? currentPlayerData
        : relativePlayerDataGetter(-1);
    if (!playerData) {
      return;
    }
    await setDarts(game.id, playerData.playerId, getScoresToUndo(playerData));
  };

  const handleFinishGame = async () => {
    await finishGame(game.id);
    setFinishDialogOpen(false);
  };

  const handleDialogUndo = async () => {
    await handleOnBack();
    setFinishDialogOpen(false);
  };

  const solution =
    SOLUTIONS[game.gameSettings.checkOut][currentPlayerRemainingScore];
  const numberOfRemainingDarts =
    currentPlayerLastDarts.length === 3 ? 3 : 3 - currentPlayerLastDarts.length;
  const hasSolution =
    solution &&
    solution.filter(isDefined).length <= numberOfRemainingDarts;

  return (
    <>
      <Flex flexDirection="column" alignItems="center" width="100%">
        <Box
          height={
            hasSolution ? "calc(100dvh - 296px - 24px)" : "calc(100dvh - 296px)"
          } // 6 (keyboard rows) * 48px (keyboard row height) + 2 * 4px (vertical padding) = 296px. Plus 24 px if a solution is shown.
          width="100%"
          maxWidth="384px"
          overflowY="scroll"
        >
          <GameStateView />
        </Box>
        {hasSolution && (
          <Box height={"24px"}>
            {solution
              .filter(isDefined)
              .map((it) => displayValueFromDartsScore(it))
              .join(" ")}
          </Box>
        )}
        <Box height="296px" width="100%" maxWidth="384px">
          <DartsKeyboard onScore={handleOnScore} onBack={handleOnBack} />
        </Box>
        <Box height="2px"></Box>
      </Flex>

      <Modal isOpen={isFinishDialogOpen} onClose={() => {}}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>{"Congratulations! You finished the game!"}</ModalHeader>
          <ModalBody>
            <Box>{"Do you want to finish the game?"}</Box>
          </ModalBody>
          <ModalFooter>
            <Button
              variant="outline"
              marginRight="20px"
              onClick={handleDialogUndo}
            >
              Undo Last Dart
            </Button>
            <Button onClick={handleFinishGame}>Finish Game</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
