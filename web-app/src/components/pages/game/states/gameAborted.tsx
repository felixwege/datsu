import { Box } from "@chakra-ui/react";

export default function GameAborted() {
  return <Box>Aborted</Box>;
}
