import { useCurrentGame } from "@/hooks/game";
import { usePlayers } from "@/hooks/players";
import { Game } from "@/models/game";
import { PlayerStats } from "@/models/stats";
import { createRematch } from "@/services/game";
import { shuffleArray } from "@/utils/array";
import { buildPlayerCheckoutStats } from "@/utils/stats";
import { Box, Button, Table, TableContainer, Tbody, Td, Tr, VStack } from "@chakra-ui/react";
import { useRouter } from "next/router";

export default function GameFinished() {
  const router = useRouter();
  const { data: game, isLoading } = useCurrentGame();

  const onClickRematch = async () => {
    const id = await createRematch(
      game?.gameSettings,
      shuffleArray(game?.playerIds ?? [])
    );

    router.push({
      pathname: `/game`,
      query: {
        gameId: id,
      },
    });
  };

  if (isLoading || !game) {
    return <Box>Loading...</Box>;
  }

  return (
    <VStack>
      <Box>Finished</Box>
      <Button variant="outline" onClick={() => router.push("/")}>
        Take me Home
      </Button>
      <Button onClick={onClickRematch}>Rematch</Button>
      <PostGameStats game={game} />
    </VStack>
  );
}

interface PostGameStatsProps {
  game: Game;
}

function PostGameStats(props: PostGameStatsProps) {
  const { game } = props;
  const { data: players, isLoading } = usePlayers(game.playerIds);
  const playerStats = Object.fromEntries(
    game.playerIds.map((playerId) => [
      playerId,
      buildPlayerCheckoutStats(playerId, [game]),
    ]),
  );

  if (isLoading || !players) {
    return (<></>);
  }


  function getPlayerName(playerIndex: number) {
    if (playerIndex < 0 || playerIndex >= game.playerIds.length) {
      return "";
    }

    if (!players) {
      return "";
    }

    const playerId = game.playerIds[playerIndex];
    const player = players[playerId];
    return player.name;
  }

  const rows = getTableRows(Object.values(playerStats)).filter((it) => it.value > 0);
  return (
    <TableContainer>
      <Table>
        <Tbody>
          {rows.map((it) => (
            <Tr key={`tr-${it.description}`}>
              <Td>{it.description}</Td>
              <Td>{getPlayerName(it.playerIndex)}</Td>
              <Td>{it.value}</Td>
            </Tr>
          ))}
        </Tbody>
      </Table>
    </TableContainer>
  );
}

function getTableRows(stats: PlayerStats[]): { description: string; value: number; playerIndex: number }[] {
  const maxScores = stats.map((it) => it.maxScore);
  const bustCounts = stats.map((it) => it.bustCount);
  const tripleTwentyCount = stats.map((it) => it.tripleTwentyCount);
  const tripleNineteenCount = stats.map((it) => it.tripleNineteenCount);
  const zeroCounts = stats.map((it) => it.zeroCount);
  const breakfastCounts = stats.map((it) => it.breakfastCount);

  const maxMaxScore = Math.max(...maxScores);
  const maxBustCounts = Math.max(...bustCounts);
  const maxTripleTwentyCount = Math.max(...tripleTwentyCount);
  const maxTripleNineteenCount = Math.max(...tripleNineteenCount);
  const maxZeroCount = Math.max(...zeroCounts);
  const maxBreakfastCount = Math.max(...breakfastCounts);

  return [
    {
      description: "Max score",
      value: maxMaxScore,
      playerIndex: maxScores.indexOf(maxMaxScore),
    },
    {
      description: "Max busts",
      value: maxBustCounts,
      playerIndex: bustCounts.indexOf(maxBustCounts)
    },
    {
      description: "Max triple 20s",
      value: maxTripleTwentyCount,
      playerIndex: tripleTwentyCount.indexOf(maxTripleTwentyCount),
    },
    {
      description: "Max triple 19s",
      value: maxTripleNineteenCount,
      playerIndex: tripleNineteenCount.indexOf(maxTripleNineteenCount),
    },
    {
      description: "Max zeros",
      value: maxZeroCount,
      playerIndex: zeroCounts.indexOf(maxZeroCount),
    },
    {
      description: "Max breakfasts",
      value: maxBreakfastCount,
      playerIndex: breakfastCounts.indexOf(maxBreakfastCount),
    },
  ];
}
