import { LoadingSpinner } from "@/components/loadingSpinner";
import { useCurrentGame } from "@/hooks/game";
import { CheckOut, Score } from "@/models/game";
import { setGameSettings } from "@/services/game";
import { formatCheckOut } from "@/utils/format";
import { Box, Select, VStack } from "@chakra-ui/react";

interface ScoreOption {
  value: Score;
  label: string;
}

interface ChekckOutOption {
  value: CheckOut;
}

export function GameSettingsSelect() {
  const { data: game, isLoading } = useCurrentGame();
  const scoreOptions: ScoreOption[] = [
    {
      value: 101,
      label: "101",
    },
    {
      value: 301,
      label: "301",
    },
    {
      value: 501,
      label: "501",
    },
  ];

  const checkOutOptions: ChekckOutOption[] = [
    {
      value: "STRAIGHT_OUT",
    },
    {
      value: "DOUBLE_OUT",
    },
    {
      value: "MASTER_OUT",
    },
  ];

  const handleSelectScore = (event: any) => {
    if (!game) {
      return;
    }

    setGameSettings(game.id, {
      ...game.gameSettings,
      score: event.target.value,
    });
  };

  const handleSelectCheckOut = (event: any) => {
    if (!game) {
      return;
    }

    setGameSettings(game.id, {
      ...game.gameSettings,
      checkOut: event.target.value,
    });
  };

  if (isLoading) {
    return <LoadingSpinner />;
  }

  if (!game) {
    return <Box>No game found</Box>;
  }

  return (
    <VStack>
      <Select
        isRequired
        value={game.gameSettings.score}
        onChange={handleSelectScore}
      >
        {scoreOptions.map((it) => (
          <option key={it.value} value={it.value}>
            {it.label}
          </option>
        ))}
      </Select>
      <Select
        isRequired
        value={game.gameSettings.checkOut}
        onChange={handleSelectCheckOut}
      >
        {checkOutOptions.map((it) => (
          <option key={it.value} value={it.value}>
            {formatCheckOut(it.value)}
          </option>
        ))}
      </Select>
    </VStack>
  );
}
