import GameCreated from "./states/gameCreated";
import GameInProgress from "./states/gameInProgress";
import GameFinished from "./states/gameFinished";
import GameAborted from "./states/gameAborted";
import { useCurrentGame } from "@/hooks/game";
import { LoginGuard } from "@/components/loginGuard";
import { Box } from "@chakra-ui/react";
import { LoadingSpinner } from "@/components/loadingSpinner";
import { urlSyncEffect } from "recoil-sync";
import { string, nullable } from "@recoiljs/refine";
import { atom, useRecoilState } from "recoil";

// TODO: This is not used correctly? We don't use it at all
const gameIdState = atom<string | null | undefined>({
  key: "HomePage.gameId",
  default: null,
  effects: [
    urlSyncEffect({
      refine: nullable(string()),
      itemKey: "gameId",
    }),
  ],
});

export default function GamePage() {
  const [_, __] = useRecoilState(gameIdState);
  const { data: game, isLoading } = useCurrentGame();

  if (isLoading) {
    return <LoadingSpinner />;
  }

  if (!game) {
    return <Box>No game found</Box>;
  }

  return (
    <LoginGuard>
      {game.state === "CREATED" && <GameCreated />}
      {game.state === "IN_PROGRESS" && <GameInProgress />}
      {game.state === "FINISHED" && <GameFinished />}
      {game.state === "ABORTED" && <GameAborted />}
    </LoginGuard>
  );
}
