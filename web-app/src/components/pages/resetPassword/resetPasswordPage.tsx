import { useState } from "react";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { userService } from "@/services/services";
import { Box, Button, HStack, Input, VStack } from "@chakra-ui/react";
import { isDefined } from "@/utils/filter";

const ResetPasswordPage = () => {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const resetPassword = async () => {
    try {
      setIsLoading(true);
      await userService.resetPassword(email);
      enqueueSnackbar({
        variant: "success",
        message: "Password reset mail sent",
      });
      router.replace("/");
    } catch (e) {
      enqueueSnackbar({ variant: "error", message: "Reset failed" });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <VStack>
      <Box>Reset Password</Box>
      <Input
        type="text"
        placeholder="E-Mail"
        onChange={(event) => setEmail(event.target.value)}
      />
      <HStack>
        <Button
          onClick={resetPassword}
          isDisabled={!isDefined(email) || email.length === 0}
        >
          Reset
        </Button>
      </HStack>
    </VStack>
  );
};
export default ResetPasswordPage;
