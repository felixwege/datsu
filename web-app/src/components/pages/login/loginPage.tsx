import { useState } from "react";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { userService } from "@/services/services";
import { Box, Button, HStack, Input, VStack } from "@chakra-ui/react";
import { isDefined } from "@/utils/filter";

const LoginPage = () => {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const login = async () => {
    try {
      setIsLoading(true);
      await userService.login(email, password);
      router.replace("/");
    } catch (e) {
      enqueueSnackbar({ variant: "error", message: "Login failed" });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <VStack>
      <Box>Login</Box>
      <Input
        type="text"
        placeholder="E-Mail"
        onChange={(event) => setEmail(event.target.value)}
      />
      <Input
        type="password"
        placeholder="Password"
        onChange={(event) => setPassword(event.target.value)}
      />
      <HStack>
        <Button
          onClick={login}
          isDisabled={
            !isDefined(email) ||
            !isDefined(password) ||
            email.length === 0 ||
            password.length === 0
          }
        >
          Login
        </Button>
        <Button
          variant={"outline"}
          onClick={() => router.push("/reset-password")}
        >
          Reset Password
        </Button>
        <Button
          variant={"outline"}
          onClick={() => router.push("/registration")}
        >
          Register
        </Button>
      </HStack>
    </VStack>
  );
};
export default LoginPage;
