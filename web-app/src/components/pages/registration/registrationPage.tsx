import { useState } from "react";
import { useRouter } from "next/router";
import { useSnackbar } from "notistack";
import { userService } from "@/services/services";
import { Box, Button, Input, VStack } from "@chakra-ui/react";
import { isDefined } from "@/utils/filter";

const RegistrationPage = () => {
  const router = useRouter();
  const { enqueueSnackbar } = useSnackbar();
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const register = async () => {
    try {
      setIsLoading(true);
      await userService.register(username, email, password);
      router.replace("/");
    } catch (e) {
      enqueueSnackbar({ variant: "error", message: "Register failed" });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <VStack>
      <Box>Registration</Box>
      <Input
        type="text"
        placeholder="Username"
        onChange={(event) => setUsername(event.target.value)}
      />
      <Input
        type="text"
        placeholder="E-Mail"
        onChange={(event) => setEmail(event.target.value)}
      />
      <Input
        type="password"
        placeholder="Password"
        onChange={(event) => setPassword(event.target.value)}
      />
      <Button
        onClick={register}
        isDisabled={
          !isDefined(username) ||
          !isDefined(email) ||
          !isDefined(password) ||
          username.length === 0 ||
          email.length === 0 ||
          password.length === 0
        }
      >
        Register
      </Button>
    </VStack>
  );
};
export default RegistrationPage;
