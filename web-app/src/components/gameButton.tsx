import { usePlayers } from "@/hooks/players";
import { Game } from "@/models/game";
import { formatCheckOut } from "@/utils/format";
import { Button, Flex, Spacer, VStack, Text } from "@chakra-ui/react";
import { useRouter } from "next/router";

export function GameButton({ game }: { game: Game }) {
  const router = useRouter();
  const { data: players, isLoading } = usePlayers(game.playerIds);
  const numberOfPlayers = Object.keys(game.players).length;

  const onOpenGame = (gameId: string) => {
    router.push({
      pathname: `/game`,
      query: {
        gameId: gameId,
      },
    });
  };

  return (
    <Button
      w="260px"
      py="30px"
      key={game.id}
      variant="outline"
      onClick={() => onOpenGame(game.id)}
    >
      <VStack
        minWidth="100%"
        align={"start"}
        direction={"column"}
        overflow={"clip"}
        spacing={"2px"}
      >
        <Flex minWidth="100%" alignItems="center" direction={"row"}>
          <Text fontSize="sm">{`${numberOfPlayers} ${numberOfPlayers > 1 ? "players" : "player"
            }`}</Text>
          <Spacer />
          <Text fontSize="xs">{`${game.gameSettings.score},`}</Text>
          <Text fontSize="xs">
            {formatCheckOut(game.gameSettings.checkOut, { short: true })}
          </Text>
          <Spacer />
          <Text fontSize={"xs"}>{game.createdAt.toRelative()}</Text>
        </Flex>
        <Text pt="2px" fontSize="xs">
          {isLoading || !players
            ? "Loading ..."
            : game.playerIds.map((id) => players[id].name).join(", ")}
        </Text>
      </VStack>
    </Button>
  );
}
