import { useRouter } from "next/router";
import React, { createContext, useEffect } from "react";
import { useRecoilValueLoadable } from "recoil";
import { userState } from "@/state/user";
import { User } from "@/models/player";
import { Box } from "@chakra-ui/react";
import { LoadingSpinner } from "./loadingSpinner";

export const UserContext = createContext<() => User>(() => {
  throw new Error("UserContext not initialized");
});

export interface LoginGuardProps {
  readonly children: React.ReactNode;
}

export const LoginGuard = (props: LoginGuardProps) => {
  const { state, contents: user } = useRecoilValueLoadable(userState);
  const router = useRouter();

  useEffect(() => {
    if (state === "hasValue" && !user) {
      router.push({ pathname: "/login" });
    }
  }, [user, router, state]);

  if (state === "loading") {
    return <LoadingSpinner />;
  }

  if (state === "hasError") {
    return <Box>Error</Box>;
  }

  if (!user) {
    return null;
  }

  return (
    <UserContext.Provider value={() => user}>
      {props.children}
    </UserContext.Provider>
  );
};
