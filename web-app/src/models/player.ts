export interface Player {
  readonly id: string; // document id of player
  readonly firebaseUuid: string; // firebase uuid
  readonly name: string;
  readonly domain: string;
}

export interface AuthUser {
  readonly id: string; // firebase uuid
  readonly email?: string;
  readonly displayName?: string;
  readonly avatarUrl?: string;
}

export interface User {
  readonly authUser: AuthUser;
  readonly player: Player;
}
