import { DateTime } from "luxon";
import { DartsScore } from "./score";

export const CHECKOUT = ["STRAIGHT_OUT", "DOUBLE_OUT", "MASTER_OUT"] as const;
export type CheckOut = (typeof CHECKOUT)[number];
export const STATS_CHECKOUT = ["COMBINED", ...CHECKOUT] as const;
export type StatsCheckout = (typeof STATS_CHECKOUT)[number];

export type Score = 101 | 301 | 501;

export interface GameSettings {
  score: Score;
  checkOut: CheckOut;
}

export interface PlayerData {
  playerId: string;
  position: number;
  darts: (DartsScore | null)[];
}

export type GameState = "CREATED" | "IN_PROGRESS" | "FINISHED" | "ABORTED";

export interface Game {
  id: string;
  gameSettings: GameSettings;
  playerIds: string[];
  players: { [playerId: string]: PlayerData };
  state: GameState;
  createdAt: DateTime;
}
