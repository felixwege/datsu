// TODO game mode specific stats
export interface PlayerStats {
  playerId: string;
  numberOfGames: number;
  numberOfWins: number;
  numberOfDarts: number;
  bustCount: number;
  average: number;
  maxScore: number;
  tripleTwentyCount: number;
  tripleTwentyRatio: number; // [0,1]
  tripleNineteenCount: number;
  tripleNineteenRatio: number;
  zeroCount: number;
  doubleRatio: number;
  tripleRatio: number;
  breakfastCount: number;
  firstNineAverage: number;
  sixtyPlusCount: number;
  hundredPlusCount: number;
  hundredFourtyPlusCount: number;
  hundredEightyCount: number;
  counts: {
    [score: string]: number;
  };
}
