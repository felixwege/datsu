import { getApps, initializeApp } from "firebase/app";

const config_dev = {
  apiKey: "AIzaSyBatIuC3RsGlfCtwm5kT7MWhZQ0RQ8aGh0",
  authDomain: "datsu-dev.firebaseapp.com",
  projectId: "datsu-dev",
  storageBucket: "datsu-dev.appspot.com",
  messagingSenderId: "1057528471280",
  appId: "1:1057528471280:web:6cf3fcb2da07b3ab426c35",
};

const config_prod = {
  apiKey: "AIzaSyCW3Us1OyoHHkjWwMPCYF6jHu5Q_Bd5AOo",
  authDomain: "datsu-prod.firebaseapp.com",
  projectId: "datsu-prod",
  storageBucket: "datsu-prod.appspot.com",
  messagingSenderId: "52331828048",
  appId: "1:52331828048:web:068679da538e66f6ad5255",
};

export function initializeFirebase() {
  if (!getApps().length) {
    if (process.env.NEXT_PUBLIC_ENVIRONMENT === "dev") {
      initializeApp(config_dev);
    }
    if (process.env.NEXT_PUBLIC_ENVIRONMENT === "prod") {
      initializeApp(config_prod);
    }
  }
}
