# Glossar

* Game: A full game with one or multiple players.
* Leg: _Not implemented yet_
* Turn: When it is a player's turn, he has up to three throws. The package of three throws is called a turn.
* Dart: When you throw an arrow its called a dart. *Currently/Sometimes* it is also used to refer to the throws that were not performed due to a bust.
* <TBD>: Only throws that have actually been performed should be called darts. We are looking for a better word for what is currently called darts.
