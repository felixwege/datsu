import { applicationDefault, initializeApp } from "firebase-admin/app";
import { getFirestore } from "firebase-admin/firestore";

async function main() {
  initializeApp({
    credential: applicationDefault(),
  });

  const db = getFirestore();

  db.runTransaction(async (tx) => {
    const ref = db.collection("games");
    const snapshots = await tx.get(ref);
    snapshots.forEach((doc) => {
      const data = doc.data();
      const players = data.players as { [playerId: string]: any } | undefined;
      if (!players) {
        return;
      }
      tx.update(doc.ref, { playerIds: Object.keys(players) });
    });
  });
}

main();
